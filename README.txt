This oversimplified example tries to make a point to several issues:
* why is it important to be careful with the compilation flags
* some other points that you should discover by yourself...

1) compile the code using

$ gcc -g  main.c aFunction.c 

and execute the code. What does happen?

2) compile the code using

$ gcc -g -Wall main.c aFunction.c

and notice the difference
